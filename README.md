# Blockchain Consensus Algorithm in Rust

This repository contains a basic implementation of a blockchain, demonstrating the core concepts of a Consensus Algorithm. The implementation is written in Rust and is an example of a Proof of Stake (PoS) consensus mechanism.

## Installation

To run this project, you need to have Rust and Cargo installed on your machine. If you haven't installed Rust and Cargo yet, follow the instructions on the [official Rust website](https://www.rust-lang.org/learn/get-started).

### Steps to Install

1. Clone the repository:
   ```bash
   git clone https://your-gitlab-repository-url.git
   cd blockchain_project
   ```

2. Build the project:
   ```bash
   cargo build
   ```

## Usage

After installing, you can run the project using Cargo:

```bash
cargo run
```

This command will execute the `main` function in the `main.rs` file, which includes the logic for adding new blocks to the blockchain and demonstrating the consensus mechanism.

## Contributing

Contributions to this project are welcome. To contribute:

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes and commit them (`git commit -am 'Add some feature'`).
4. Push to the branch (`git push origin feature-branch`).
5. Create a new Pull Request.

Please make sure to update tests as appropriate.

## License

[MIT](https://opensource.org/licenses/MIT)

