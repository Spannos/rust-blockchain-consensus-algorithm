use rand::Rng;
use std::collections::HashMap;

#[derive(Debug)]
// Define the structure for a block
struct Block {
    data: String,
    hash: String,
    prev_hash: String,
    validator: String,
}

// Implementing basic functionalities for the block
impl Block {
    // Constructor for a new block
    fn new(data: String, prev_hash: String, validator: String) -> Block {
        Block {
            data,
            hash: String::new(),
            prev_hash,
            validator,
        }
    }

    // Method to calculate the hash of the block
    fn calculate_hash(&self) -> String {
        let contents = format!("{}{}{}", &self.data, &self.prev_hash, &self.validator);
        format!("{:x}", md5::compute(contents))
    }
}

// Define the structure for the blockchain
struct Blockchain {
    chain: Vec<Block>,
    nodes: HashMap<String, u64>, // Storing nodes and their stakes
}

// Implementing basic functionalities for the blockchain
impl Blockchain {
    // Constructor for a new blockchain
    fn new() -> Blockchain {
        let genesis_block = Block::new("Genesis Block".to_string(), String::new(), String::new());
        Blockchain {
            chain: vec![genesis_block],
            nodes: HashMap::new(),
        }
    }

    // Method to register a new node with its stake
    fn register_node(&mut self, node_id: String, stake: u64) {
        self.nodes.insert(node_id, stake);
    }

    // Method to add a new block
    fn add_block(&mut self, data: String) {
        let validator = self.select_validator();
        let prev_hash = self.chain.last().unwrap().hash.clone();
        let mut new_block = Block::new(data, prev_hash, validator);
        new_block.hash = new_block.calculate_hash();
        self.chain.push(new_block);
    }

    // Select a validator based on the stake
    fn select_validator(&self) -> String {
        let mut rng = rand::thread_rng();
        let total_stake: u64 = self.nodes.values().sum();

        // Selecting a random stake target within the total stake
        let mut stake_target = rng.gen_range(0..total_stake);
        for (node, &stake) in &self.nodes {
            if stake_target < stake {
                return node.clone();
            }
            stake_target -= stake;
        }

        panic!("Failed to select a validator")
    }

    // Method to validate the integrity of the blockchain
    fn is_valid(&self) -> bool {
        for i in 1..self.chain.len() {
            let current_block = &self.chain[i];
            let prev_block = &self.chain[i - 1];

            if current_block.hash != current_block.calculate_hash() {
                return false;
            }

            if current_block.prev_hash != prev_block.hash {
                return false;
            }
        }
        true
    }
}

fn main() {
    let mut blockchain = Blockchain::new();

    // Registering nodes with their stakes
    blockchain.register_node("Node1".to_string(), 100);
    blockchain.register_node("Node2".to_string(), 50);

    blockchain.add_block("Block 1 Data".to_string());
    blockchain.add_block("Block 2 Data".to_string());

    println!("Blockchain valid? {}", blockchain.is_valid());
    for block in blockchain.chain {
        println!("{:?}", block);
    }
}

